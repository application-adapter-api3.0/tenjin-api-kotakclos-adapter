package com.yethi.tenjin.kotakclos;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.yethi.tenjin.api.models.PageArea;
import com.yethi.tenjin.api.models.ValidationResult;
import com.yethi.tenjin.api.models.Validparam;
import com.yethi.tenjin.executor.ActionResult;
import com.yethi.tenjin.sdk.api.adapter.ApiInterceptor;
import com.yethi.tenjin.sdk.api.adapter.DefaultInterceptor;
import com.yethi.tenjin.sdk.api.adapter.models.ClientRequest;
import com.yethi.tenjin.sdk.api.adapter.models.ClientResponse;
import com.yethi.tenjin.xmlutils.SampleCheck;

import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ApplicationImpl extends DefaultInterceptor implements ApiInterceptor {

	Map<String, String> namespaces = new HashMap<String, String>();
	static Map<String, String> value = new HashMap<String, String>();
	List<String> outputlabel = new ArrayList<String>();
	
	String encryptionKey = "";

	@Override
	public void preRequest(ClientRequest request,List<PageArea> pages) throws ParserConfigurationException,TransformerException,IOException{
		String payload = null;
		log.info("Entering preRequest to xml data");
		String xml=request.getPayload();
		//String xml = request.getPayload();
		if(!request.getApi().getApiOperation().getRequestRepresentation().getMediaType().equals("application/json")) {
		xml	= new SampleCheck().convertJsonToXml(request.getPayload());
		//String xml = new JsonToXmlConverter().convrtJsonToXml(request.getApi().getApiOperation().getRequestRepresentation().getRepresentationDescription(),request.getPayload());
		try {
			log.info("Adding namespace");
			xml=addNamespaceToXml(xml);
		} catch (ParserConfigurationException | SAXException | IOException | TransformerException e1) {
		}
		}
		request.setRawPayload(xml);
		encryptionKey = request.getApi().getEncryptionKey();
		try {
			payload = CryptoUtils.encrypt(xml, encryptionKey);
			request.setPayload(payload);
			log.info("Converted to xml data");
			log.info("------------------------");
			log.info(payload);
			log.info("------------------------");
		} catch (Exception e) {
			log.error("Could Not build the request in ApplicationImpl Api");
			throw new IOException("Could Not build the request in ApplicationImpl Api");
		}
	}

	@Override
	public void postResponse(ClientResponse response) {
		String xmlResponseData = null;
		try {
			log.info("only for xml api");
			try {
				xmlResponseData = CryptoUtils.decrypt(response.getResponseData().toString(), encryptionKey);
			} catch (Exception e) {
				xmlResponseData=response.getResponseData().toString();
			}
			response.setResponseData(xmlResponseData);
			if(xmlResponseData.contains("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>")) {
				xmlResponseData=xmlResponseData.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>","");
			}
			log.info("------------------------");
			log.info(xmlResponseData);
			log.info("------------------------");
			response.setReponseDateLableAndValue(this.labelFinderforxml(xmlResponseData));
		} catch (Exception  e) {
		}

		/*added to find the label JSONformt*/
		
		/*--->setting validation result<---*/
		if (response.getValidationResult() != null) {
            response.getReponseDateLableAndValue();
            List<ValidationResult> valid = response.getValidationResult();
            List<Validparam>validparams=response.getParamValue();
            for (int i = 0; i < valid.size(); i++) {
                ValidationResult v = valid.get(i);
                
				for (int k = 0; k < validparams.size(); k++) {
					Validparam validparam = validparams.get(k);
					// if(validparam.getFeidname().contains(v.getField()))
					if (v.getPage().equals(validparam.getPage()) && v.getField().equals(validparam.getFeidname())) {
						// check for the page....
						validparam.getValue();
						v.setActualValue(validparam.getValue());
					}
				}
                try {
                if (v.getActualValue().equals(v.getExpectedValue())) {
                    v.setStatus(ActionResult.S);
                } else {
                    v.setStatus(ActionResult.F);
                }
                }
                catch (NullPointerException e) {
                    log.info("could not set value because of {}",e);
                }
           }
        }

		int status = response.getStatus();
		if (status == 201||status==200) {
			response.setResult(ActionResult.S);
			response.setRemarks("PASS");
			
			try {
				Map<String, String> msg = new HashMap<String, String>();
				msg = response.getReponseDateLableAndValue();
				
				//msg.get("Msg").equalsIgnoreCase("please");
					if(msg.get("ErrorCode")!=null||msg.get("ns0:ErrorCode")!=null||msg.get("ns0:Code")!=null) {
				response.setResult(ActionResult.F);
				
				response.setRemarks(msg.get("ErrorCode")!=null? msg.get("ErrorCode"):msg.get("ns0:ErrorCode"));
					} 
			} catch (Exception e) {
				if(response.getResponseData().toString().contains("ErrorCode")) {
					response.setResult(ActionResult.F);
					response.setRemarks("Failed");
				}else {
				response.setResult(ActionResult.S);
				response.setRemarks("PASS");
				}
			}
		} else
			response.setResult(ActionResult.F);

	}

	public Map<String, String> labelFinderforxml(String file)
			throws ParserConfigurationException, SAXException, IOException, TransformerException {
		DocumentBuilderFactory docbuildfac = DocumentBuilderFactory.newInstance();

		// we are creating an object of builder to parse
		// the xml file.
		DocumentBuilder db = docbuildfac.newDocumentBuilder();
		Document doc = null;
		doc = db.parse(new InputSource(new StringReader(file)));
		//System.out.println("Root element: " + doc.getDocumentElement().getNodeName());

		if (doc.getDocumentElement().hasChildNodes()) {
			NodeList nodeList = doc.getDocumentElement().getChildNodes();
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node childNode = nodeList.item(i);
				if (childNode.getNodeType() == Node.ELEMENT_NODE) {
					doSomething(childNode);
				}
			}
		}
		return value;
	}

	public static void doSomething(Node node) {
		Element element = (Element) node;
		List<Validparam>parameter=new ArrayList<Validparam>();
		//Map<String, String> value1 = new HashMap<String, String>();
		try {
				parameter.add(new Validparam(node.getParentNode().getNodeName(), node.getNodeName(),element.getFirstChild().getNodeValue()));
				if(!value.containsKey(node.getNodeName())) {
				value.put(node.getNodeName(), element.getFirstChild().getNodeValue());
				}
				else {
					for(int i=2;i<=10;i++) {
						if(value.containsKey(node.getNodeName())) {
							
						String dup=node.getNodeName()+" "+"0"+i;
						if(!value.containsKey(dup))
						{
							value.put(dup, element.getFirstChild().getNodeValue());
							break;
						}
						}
					}
				}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		NodeList nodeList = node.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node currentNode = nodeList.item(i);
			if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
				doSomething(currentNode);
			}
		}
	}
	
	public Map<String, String> labelForJson(String jsonresponse) {
		JSONObject obj = new JSONObject(jsonresponse);
		Map<String, String> value2 = new HashMap<String, String>();
		Set<String> jsons = obj.keySet();
		for (String json : jsons) {
			value.put(json, obj.getString(json));

		}
		return value;
	}
	
	public static String filterDecode(String url){

		   url = url.replaceAll("&amp;", "&")
		     .replaceAll("&lt;", "<")
		     .replaceAll("&gt;", ">")
		     .replaceAll("&apos;", "\'")
		     .replaceAll("&quot;", "\"")
		     .replaceAll("&nbsp;", " ")
		     .replaceAll("&copy;", "@")
		     .replaceAll("&reg;", "?")
		     .replaceAll("&quot;", "<")
		     .replaceAll("&quot;:", ">");
		   return url;
	 }
	@Override
	public void validateField(ClientResponse response) {
		if (response.getValidationResult() != null) {
            response.getReponseDateLableAndValue();
            List<ValidationResult> valid = response.getValidationResult();
            List<Validparam>validparams=response.getParamValue();
            for (int i = 0; i < valid.size(); i++) {
                ValidationResult v = valid.get(i);
                
				for (int k = 0; k < validparams.size(); k++) {
					Validparam validparam = validparams.get(k);
					if (v.getPage().equals(validparam.getPage()) && v.getField().equals(validparam.getFeidname())) {
						validparam.getValue();
						v.setActualValue(validparam.getValue());
						break;
					}
				}
                try {
                if (v.getActualValue().trim().equals(v.getExpectedValue())) {
                    v.setStatus(ActionResult.S);
                } else {
                    v.setStatus(ActionResult.F);
                    response.setResult(ActionResult.F);
        			response.setRemarks("Failed due to validation fail");
                } } catch (NullPointerException e) {
                    log.info("could not set value because of {}",e);
                }
           }
        }
	}
	private static String addNamespaceToXml(String in)
	        throws ParserConfigurationException, SAXException, IOException,
	        TransformerException {
	    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	    DocumentBuilder db = dbf.newDocumentBuilder();
	   
	    Document document = db.parse(new InputSource(new StringReader(in)));
	    Element documentElement = document.getDocumentElement();
	    if(documentElement.toString().contains("CreateLeadReq")) {
	    documentElement.setAttribute("xmlns", "http://www.kotak.com/schemas/AggregatorLeadCRM/CreateLeadReq");
	    String xml = transformXmlNodeToXmlString(documentElement);
	    return xml;
	    }else if(documentElement.toString().contains("CLOSRequest")) {
	    	documentElement.setAttribute("xmlns", "http://www.kotak.com/schemas/Crisil/CLOSRequest");
		    String xml = transformXmlNodeToXmlString(documentElement);
		    return xml;
	    }else if(documentElement.toString().contains("KCBLMS_BL")) {
	    	documentElement.setAttribute("xmlns", "http://www.kotak.com/FICO/KCBLMS_BL");
		    String xml = transformXmlNodeToXmlString(documentElement);
		    return xml;
	    }else if(documentElement.toString().contains("cpid_creation")||documentElement.toString().contains("cpid_pull")) {
	    	String xml = in.replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>","").trim();
		    return xml;
	    }
	    return in;
	}
	private static String transformXmlNodeToXmlString(Node node)
	        throws TransformerException {
	    TransformerFactory transFactory = TransformerFactory.newInstance();
	    Transformer transformer = transFactory.newTransformer();
	    StringWriter buffer = new StringWriter();
	    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
	    try {
			transformer.transform(new DOMSource(node), new StreamResult(buffer));
		} catch (TransformerException e) {
		}
	    String xml = buffer.toString();
	    return xml;
	}
}